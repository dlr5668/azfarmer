math.randomseed( os.time() )
local number_of_simulations = 100 * 1000
local total_rolls = 0
local event_duration = 14
for i = 1, number_of_simulations do
    local ship1_rate_start = 0
    local ship1_rate_end = ship1_rate_start + 0.02
    local found_ship1 = 0
    local ship2_rate_start = ship1_rate_end
    local ship2_rate_end = ship2_rate_start + 0.02
    local found_ship2 = 0
    local ship3_rate_start = ship2_rate_end
    local ship3_rate_end = ship3_rate_start + 0.025
    local found_ship3 = 0
    local n = 0
    while found_ship1 == 0 or found_ship2 == 0 or found_ship3 == 0 do
        n = n + 1
        local roll = math.random()
        if roll > ship1_rate_start and roll < ship1_rate_end then
            found_ship1 = found_ship1 + 1
        elseif roll > ship2_rate_start and roll < ship2_rate_end then
            found_ship2 = found_ship2 + 1
        elseif roll > ship3_rate_start and roll < ship3_rate_end then
            found_ship3 = found_ship3 + 1
        end
    end
    total_rolls = total_rolls + n
end
local average_rolls = total_rolls / number_of_simulations
local cubes_needed = average_rolls * 2
print("average rolls required "..average_rolls)
local cubes_per_week = 4 * 7 + 12 + 1 * 7
local total_cubes_gain_for_event = event_duration * (cubes_per_week / 7)
local have_cubes_on_day_1 = cubes_needed - total_cubes_gain_for_event
print("start event with "..have_cubes_on_day_1.. " cubes and do "..(average_rolls / event_duration).." rolls every day")
