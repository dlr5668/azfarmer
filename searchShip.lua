M = {} -- define the module
--zone with ? and 3 buttons
UiZone1 = Region(640, 640, 1280-640, 720-640)
UiZone2 = Region(110, 0, 1280, 50)
-- ammo to refill charges
ammoDx = 0
ammoDy = 80
-- ? with loot
questionDx = 0
questionDy = 80
-- regular ship detection (LV text)
shipDx = 60
shipDy = 60
-- boss (eyes)
bossDx = 0
bossDy = 0
-- treasure ship
treasureDx = 0
treasureDy = 0
-- arrow over our ship
arrowDx = 0
arrowDy = 228

function InRegion(x, y, region)
    local result = x >= region.x and x <= region.x + region.w
        and y >= region.y and y <= region.y + region.h
    if result then
        stateMachine:log("!saved click on bad spot")
    end
    return result
end

function IsEmpty(s)
    return s == nil or s == ""
end

function NotEmpty(s)
    return not IsEmpty(s)
end

function HighlightAll(list)
    for _, m in pairs(list) do
        Highlight(m)
    end
end

---Draw temporary rectangle over object
function Highlight(object)
    if NotEmpty(HIGHLIGHT) and HIGHLIGHT and NotEmpty(object) then
        if object.x ~= nil and object.y ~= nil and
                object.w ~= nil and object.h ~= nil then
            object:highlight(3)
        end
    end
end

function CanClickHere(position)
    if InRegion(position.x, position.y, UiZone1) then return false end
    if InRegion(position.x, position.y, UiZone2) then return false end
    return true
end

-- inserts b into a
function Insert(bigTable, object, dx, dy)
    if not bigTable or not object then return end
    local randX = math.random(-5,5)
    local randY = math.random(-5,5)
    if typeOf(object) == "table" then
        for _, m in ipairs(object) do
            local realPos = m:offset(dx+randX, dy+randY)
            if CanClickHere(realPos) then
                table.insert(bigTable, realPos)
            end
        end
    end
end

function GetFirst(object)
    if typeOf(object) == "table" then
        for _, m in ipairs(object) do
           return m
        end
    end
end

function Dist(x1, y1, x2, y2)
    return math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))
end

function Dist(p1, p2)
    if not p1 or not p2 or not p1.x or not p1.y or not p2.x or not p2.y then
        return 100500
    end
    return math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y))
end

---AttackShips iterate over all ships and click on them all
function M.AttackShips()
    stateMachine:log("..searching ships")
    --swap to boss fleet if we need
    local bossArray = findAllNoFindException(Pattern("btn_boss_formation.png"))
    usePreviousSnap(true)
    if SWAP and #bossArray > 0 and stateMachine:getState() == "strategy_view_fleet1" then
        stateMachine:log("..swapping to BOSS fleet ")
        usePreviousSnap(false)
        return stateMachine:goto("strategy_view_fleet2")
    end
    --continue with rest
    local treasureArray = findAllNoFindException(Pattern("s_treasure.png"))
    local extraArray = findAllNoFindException(Pattern("s_my_hero_1.png"))
    local shipMethod3Array = findAllNoFindException(Pattern("s_strategy_view_1.png"))
    local shipMethod2Array = findAllNoFindException(Pattern("s_strategy_view.png"))
    local shipMethod1Array = findAllNoFindException(Pattern("s_strategy_view_2.png"))
    stateMachine:log("[***] "..#shipMethod3Array.." | ".."[**] "..#shipMethod2Array.." | ".."[*] "..#shipMethod1Array)
    local allTargets = {}
    -- boss (clicking again) -> treasureArray -> ? mark -> regular ship -> ammo
    local target = nil
    local clickTarget = nil
    if #bossArray > 0 then
        stateMachine:log("[p] boss")
        target = GetFirst(bossArray):offset(bossDx, bossDy)
        clickTarget = true
    elseif #extraArray > 0 then
        stateMachine:log("[p] ?")
        target = GetFirst(extraArray):offset(questionDx, questionDy)
        clickTarget = true
    elseif #treasureArray > 0 then
        stateMachine:log("[p] treasure")
        target = GetFirst(treasureArray):offset(treasureDx, treasureDy)
        clickTarget = true
    else
        target = stateMachine:find("s_map_34_1.png")
        if target then
            target = target:offset(arrowDx, arrowDy)
        end
        clickTarget = false
    end
    Insert(allTargets, bossArray, bossDx, bossDy)
    Insert(allTargets, treasureArray, treasureDx, treasureDy)
    Insert(allTargets, extraArray, questionDx, questionDy)
    Insert(allTargets, shipMethod1Array, shipDx, shipDy)
    Insert(allTargets, shipMethod2Array, shipDx, shipDy)
    Insert(allTargets, shipMethod3Array, shipDx, shipDy)
    HighlightAll(allTargets)
    --sorting
    if target then
        table.sort(allTargets, function(a, b)
            return Dist(a, target) < Dist(b, target)
        end)
    end
    if clickTarget and target then click(target) end
    for _, m in ipairs(allTargets) do
        stateMachine:log(Dist(m, target))
        click(m)
    end
    usePreviousSnap(false)
    return true
end

return M