-- global variables
ROOT = scriptPath()
AUTO_IMAGE_PATH = ROOT.."image" -- path with auto generated images
MANUAL_IMAGE_PATH = ROOT.."manual" -- manual captured stuff
HIGHLIGHT = false

-- initializing
setImagePath(AUTO_IMAGE_PATH)
dofile(ROOT.."stateMachine.lua")
local searchShip = require(ROOT.."searchShip")

-- initializing non trivial connection
stateMachine:register("bad_connection", "[prev-screen]", function ()
  wait(10)
  return stateMachine:waitKnownState(10)
end)
stateMachine:register("loading_1", "[prev-screen]", function ()
  wait(10)
  return stateMachine:waitKnownState(10)
end)
stateMachine:register("loading_2", "[prev-screen]", function ()
  wait(10)
  return stateMachine:waitKnownState(10)
end)
stateMachine:register("loading_2", "[prev-screen]", function ()
  wait(10)
  return stateMachine:waitKnownState(10)
end)

stateMachine:register("fight", "mission_result_1", function ()
  -- battle usually takes 60-100s
  stateMachine:leaveState("fight", 120)
  return stateMachine:waitKnownState(10)
end)

stateMachine:register("strategy_view_fleet1", "formation", function ()
  return strategy_viewToFormation()
end)
stateMachine:register("strategy_view_fleet2", "formation", function ()
  return strategy_viewToFormation()
end)

function strategy_viewToFormation()
  local aligned = alignM32() or alignM34() or alignM41() or alignM42() or alignM43()
    or alignM44() or alignM52() or alignM62() or alignM72()
  searchShip.AttackShips()
  wait(10)
  return stateMachine:waitStates(
          {"strategy_view_fleet1", "strategy_view_fleet2", "formation", "ambush"}, 5)
end

function alignM32()
  if target == "m32" then
    if not matchObject("s_map_32.png", Location(930, 270)) then
      matchObject("s_map_32_1.png", Location(300, 300))
    end
    return true
  end
  return false
end

function alignM34()
  if target == "m34" then
    if not matchObject("s_map_34.png", Location(810+100, 250)) then
      if matchObject("s_map_34_1.png", Location(200+100, 330)) then
        matchObject("s_map_34.png", Location(810+100, 250))
      end
    end
    return true
  end
  return false
end

function alignM41()
  if target == "m41" then
    matchObject("s_map_41.png", Location(650, 300))
    return true
  end
  return false
end

function alignM42()
  if target == "m42" then
    if not matchObject("s_map_42.png", Location(330, 507-45)) then
      matchObject("s_map_42_1.png", Location(421, 128-45))
    end
    return true
  end
  return false
end

function alignM43()
  if target == "m43" then
    matchObject("s_map_43.png", Location(229, 446))
    stateMachine:dragFind("s_map_43_1.png", -1, nil)
    return true
  end
  return false
end

function alignM44()
  if target == "m44" then
    if matchObject("s_map_44.png", Location(335+50, 163)) then return true end
    if matchObject("s_map_44_1.png", Location(703+50, 414)) then return true end
    return true
  end
  return false
end

function alignM52()
  if target == "m52" then
    if matchObject("s_map_52.png", Location(768, 252)) then return true end
    if matchObject("s_map_52_1.png", Location(382, 166)) then return true end
    return true
  end
  return false
end

function alignM62()
  if target == "m62" then
    if matchObject("s_map_62.png", Location(573, 271)) then return true end
    return true
  end
  return false
end

function alignM72()
  if target == "m72" then
    if matchObject("s_map_72.png", Location(979, 460)) then return true end
    if matchObject("s_map_72_1.png", Location(316, 189)) then return true end
    --arrow
    if matchObject("s_map_34_1.png", Location(1200, 0)) then return true end
    return true
  end
  return false
end

-- trying to match find object position with required
function matchObject(name, location)
  local object = stateMachine:find(name)
  if not object then return false end
  local mid = Location(stateMachine.wholeWindow:getW() * 0.5, stateMachine.wholeWindow:getH() * 0.5)
  local xOffset = object.x - location.x
  local yOffset = object.y - location.y
  local squareDist = xOffset * xOffset + yOffset * yOffset
  if squareDist > 40 * 40 then
    swipe(mid, Location(mid.x - 1.65 * xOffset, mid.y - 1.65 * yOffset))
  end
  return true
end

stateMachine:register("mission_result_2", "[any]", function ()
  stateMachine:log("..mission ended")
  --close
  stateMachine:click("btn_mission_result_2_strategy_view.png", 5)
  wait(10)
  return stateMachine:waitKnownState()
end)

stateMachine:register("m32", "mission_overview", function ()
  return stateMachine:goto("select_fleet_1")
end)

stateMachine:register("m34", "mission_overview", function ()
  return stateMachine:goto("select_fleet_1")
end)

stateMachine:register("m41", "mission_overview", function ()
  return stateMachine:goto("select_fleet_1")
end)

stateMachine:register("m42", "mission_overview", function ()
  return stateMachine:goto("select_fleet_1")
end)

stateMachine:register("m43", "mission_overview", function ()
  return stateMachine:goto("select_fleet_1")
end)

stateMachine:register("m44", "mission_overview", function ()
  return stateMachine:goto("select_fleet_1")
end)

stateMachine:register("m52", "mission_overview", function ()
  return stateMachine:goto("select_fleet_1")
end)

stateMachine:register("m62", "mission_overview", function ()
  return stateMachine:goto("select_fleet_1")
end)

stateMachine:register("m72", "mission_overview", function ()
  return stateMachine:goto("select_fleet_1")
end)

-- main program
SWAP = true
target = "m62"

local alive = true
local do_quests = true
while true do
    local starterState = stateMachine:getState()
    if starterState == "formation_pvp" or starterState == "pvp_1" or starterState == "pvp_2" then
      target = "pvp"
    end
    if target == "pvp" then
      alive = stateMachine:goto("pvp_1", "pvp_2", "formation_pvp", "mission_result_1")
    elseif target == "q" then --testing
      while do_quests do
        do_quests = stateMachine:goto("commission_daily_1")
        local foundQuest = stateMachine:click("s_quest_1.png")
        if foundQuest then
          do_quests = stateMachine:goto("commission_setup_1", "commission_setup_2", "commission_setup_3")
        end
        do_quests = stateMachine:goto("commission_daily_2")
        foundQuest = stateMachine:click("s_quest_1.png")
        if foundQuest then
          do_quests = stateMachine:goto("commission_setup_1", "commission_setup_2", "commission_setup_3")
        end
      end
    else
        alive = stateMachine:goto(target)
    end
end
--playMusic("ended.mp3", false)